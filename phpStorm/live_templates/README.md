## Live templates - это средство быстрой вставки кусков кода

На данный момент реализованы следующие шаблоны

Для импорта этих шаблоно к себе в систему нужно скопировать файлы в 

C:/Users/**USER**/.PhpStorm**VERSION**/config/templates/

И перезапустить PhpStorm



Контекст   |  Команда  |  Что делает
-----------|-----------|------------------
JS         |   jaja    | Вставляет код jQuery AJAX
JS         |   jajaf   | Вставляет код jQuery AJAX с обработчиком формы
JS         |   clog    | console.log
JS         |   don     | $(document).on('click')
HTML       |   prew    | var_dump обрамленный тегами PRE и вызовом PHP
PHP        |   prew    | var_dump обрамленный тегами PRE
PHP        |   PROLOG  | подлючение пролога битрикс
PHP        | component | болванка файла component.php в процедурном стиле
PHP        | binclude  | вставка включаемой области битрикс
PHP        | cinclude  | вставка компонента без названия и параметров
PHP        |  params   | болванка файла .parameters.php битрикс
PHP        |   bread   | хлебные крошки битрикс
PHP        | template  | болванка шаблона компонента

