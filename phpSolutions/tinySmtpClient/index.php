<?php

//https://docs.microsoft.com/ru-ru/exchange/mail-flow/test-smtp-with-telnet?view=exchserver-2019
while (ob_end_clean()) {
}; // на всякий случай
ob_implicit_flush(1);
function dd($d, $die = true)
{
	echo '<pre>';
	var_dump($d);
	echo '</pre>';

	if ($die) {
		die();
	}
}

/**
 * Connecto to an SMTP and send the given message
 */
function smtp_mail($to, $from, $message, $user, $pass, $host, $port)
{
	$result = ['success' => false, 'message' => 'fsockopen error'];
	if ($h = fsockopen($host, $port)) {
		$data = array(
			"EHLO $host",
			'AUTH LOGIN',
			base64_encode($user),
			base64_encode($pass),
			"MAIL FROM: <$from>",
			"RCPT TO: <$to>",
			'DATA',
			$message
		);
		$last = count($data);

		$code = 0;
		foreach ($data as $i => $c) {
			echo "command: " . htmlspecialchars($c) . "<br>";
			$c && fwrite($h, "$c\r\n");
			$data = [];
			while ($dd = trim(fgets($h))) {
				file_put_contents('smtp.log', $dd . "\n", FILE_APPEND);
				$data[] = $dd;
			}
			if ($last == ($i + 1)) {
				$code = substr(trim(end($data)), 0, 3);
				$message = trim(end($data));
			}
			echo "output:<br>" . implode("<br>", $data) . "<br>";
			echo '--------------------<br>';
		}

		fwrite($h, "QUIT\r\n");
		fclose($h);

		if (250 === (int)$code) {
			$result = ['success' => true];
		} else {
			$result = ['success' => false, 'message' => $message];
		}
	}

	return $result;
}

ini_set('default_socket_timeout', 3);
$host = '***';
$port = '***';
$user = '***';
$pass = '***';
$from = '***';
$to   = '***';
$msg = [];
$msg[] = '<h1>Test Email</h1>';
$msg[] = '<p>Hello There!</p>';
$message = implode("", $msg);
$template = "Subject: тема\r\n"
	. "To: <$to>\r\n"
	. "From: <$from>\r\n"
	. "MIME-Version: 1.0\r\n"
	. "Content-Type: text/html; charset=utf-8\r\n"
	. "\r\n" . $message . "\r\n.";

$result = smtp_mail($to, $from, $template, $user, $pass, $host, $port);
if ($result['success']) {
	echo '<p style="color: green">Mail sent done. To ' . $to . '</p>';
} else {
	echo '<p style="color: red">Mail sent fail. ' . $result['message'] . '</p>';
}
