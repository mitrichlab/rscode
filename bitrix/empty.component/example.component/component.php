<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 19.02.2019
 * Time: 20:05
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if($this->StartResultCache(false, array($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
    //$this->AbortResultCache(); //Раскомментируем эту строку чтобы отключить кэширование в компоненте

    $this->IncludeComponentTemplate();
}