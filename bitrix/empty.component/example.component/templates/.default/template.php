<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 19.02.2019
 * Time: 20:06
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false); //true включить композитный режим, false выключить
?>
<div class="example_component">
    <h3>Я начинка компонента</h3>
    <p>
        Подключаю в себя CSS и JS которые лежат рядом
    </p>
    <p>Параметр <b>PARAM_ONE</b> - <?=$arParams['PARAM_ONE']?></p>
    <p>Параметр <b>PARAM_TWO</b> - <?=$arParams['PARAM_TWO']?></p>
</div>
