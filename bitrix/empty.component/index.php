<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 19.02.2019
 * Time: 20:05
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->IncludeComponent(
    'redsoft:example.component',
    '.default',
    [
        'PARAM_ONE' => 'Param one value',
        'PARAM_TWO' => 'Param two value',
    ],
    false
);