<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 05.07.2017
 * Time: 11:29
 */

function custom_mail($to, $subject, $message, $additional_headers = '', $additional_parameters = '')
{
    $host = 'ssl://smtp.yandex.ru';
    $smtpServerHostPort      = 465;
    $smtpServerUser         = 'username';
    $smtpServerUserPassword   = 'password';

    $body = $message;

    /* Create a new Net_SMTP2 object. */
    if (! ($smtp = new Net_SMTP2($host, $smtpServerHostPort))) {
        AddMessage2Log('Unable to instantiate Net_SMTP2 object');
    }

    /* Connect to the SMTP server. */
    try {
        $smtp->connect();
    } catch (PEAR_Exception $e) {
        AddMessage2Log($e->getMessage());
        die;
    }
    $smtp->auth($smtpServerUser,$smtpServerUserPassword);
    /* Send the 'MAIL FROM:' SMTP command. */

    //preg_replace('/Subject: (.+)\n/i','',$additional_headers);
    preg_match('/From: (.+)'.PHP_EOL.'/i', $additional_headers, $matches);
    list(, $from) = $matches;

    try {
        $smtp->mailFrom(trim($from));
    } catch (PEAR_Exception $e) {
        AddMessage2Log("Unable to set sender to <$from>");
        die;
    }

    /* Address the message to each of the recipients. */
    $to_arr = explode(',',$to);

    //Разберем заголовки СС и BCC
    preg_match_all('/CC: (.+)'.PHP_EOL.'/i', $additional_headers, $matches2);
    list(, $cc) = $matches2;

    foreach ($cc as $item_string)
    {
        $expl = explode(',',$item_string);
        foreach ($expl as $eml) {
            $to_arr[] = $eml;
        }
    }

    $to_arr = array_unique(array_filter($to_arr));

    try {
        foreach ($to_arr as $to) {
            $smtp->rcptTo(trim($to));
        }
    } catch (PEAR_Exception $e) {
        AddMessage2Log("Unable to add recipient <$to>: " . $e->getMessage());
        die;
    }

    $eol = CAllEvent::GetMailEOL();
    $additional_headers .= $eol . 'To: ' . $to_arr[0];
    $additional_headers .= $eol . 'Subject: ' . $subject;

    /* Set the body of the message. */
    try {
        $smtp->data($body, $additional_headers);
    } catch (PEAR_Exception $e) {

        AddMessage2Log($e->getMessage());
    }

    /* Disconnect from the SMTP server. */
    $smtp->disconnect();
}
