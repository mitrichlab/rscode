<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 10.09.2019
 * Time: 12:02
 */
namespace rsclass;

/**
 * Class rsAjax
 * @package rsclass
 */
class rsAjax {
    private $response;
    private $add_param_type = 'prepend';

    /**
     * @param int $state
     * @param string $text
     * @param string $html
     * @param string $action
     * @param bool $write_to_log
     * @return bool
     * @throws rsExceptionLog
     */
    public function isAjax($state = 0, $text = 'Not XHR', $html = '', $action = 'break', $write_to_log = false)
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        } else {
            if($write_to_log)
            {
                $log = \rsclass\rsLog::getInstance();
                $log->write('Not XHR');
            }
            if($action == 'break')
            {
                $this->setState($state);
                $this->setText($text);
                $this->setHtml($html);
                $this->showResponse();
            } else {
                throw new \Exception('Request is not AJAX', 5007);
            }
        }
        return false;
    }

    /**
     * @param string $type
     */
    public function setAddParamType($type = 'prepend')
    {
        if($type == 'prepend')
        {
            $this->add_param_type = $type;
        } else {
            $this->add_param_type = 'append';
        }
    }

    /**
     * @param string $type
     */
    public function setHeader($type = 'json')
    {
        header("Content-Type: text/$type");
    }

    /**
     * @param $state
     */
    public function setState($state)
    {
        $this->state = $state;
        $this->response['state'] = $state;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return isset($this->response['state']) ? $this->response['state'] : null;
    }

    /**
     * @param $text string
     */
    public function setText($text)
    {
        $this->response['text'] = $text;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return isset($this->response['text']) ? $this->response['text'] : null;
    }

    /**
     * @param $html string
     */
    public function setHtml($html)
    {
        $this->response['html'] = $html;
    }

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return isset($this->response['html']) ? $this->response['html'] : null;
    }

    /**
     * @param $name string
     * @param $value mixed
     */
    public function __set($name, $value)
    {
        if($this->add_param_type == 'prepend')
        {
            $this->response = array_reverse($this->response, true);
        }

        $this->response[$name] = $value;

        if($this->add_param_type == 'prepend')
        {
            $this->response = array_reverse($this->response, true);
        }
    }

    /**
     * @param $name string
     * @return mixed
     */
    public function __get($name)
    {
        return (isset($this->response[$name])) ? $this->response[$name] : null;
    }

    /**
     * @param $name string
     */
    public function __unset($name)
    {
        if(isset($this->response[$name]))
        {
            unset($this->response[$name]);
        }
    }

    public function __isset($name)
    {
        return isset($this->response[$name]);
    }

    /**
     * @param bool $exit
     */
    public function showResponse($exit = true)
    {
        echo json_encode($this->response);
        if($exit)
        {
            exit;
        }
    }

    public function getResponse()
    {
        return $this->response;
    }
}