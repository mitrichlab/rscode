<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 09.07.2017
 * Time: 22:32
 */

//Errors 30**
namespace rsclass;


trait rsTraitFile
{
    /**
     * @param $full_path
     */
    public static function removeRecursive($full_path)
    {
        die('temporary disabled');
        $it = new RecursiveDirectoryIterator($full_path);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->getFilename() === '.' || $file->getFilename() === '..') {
                continue;
            }
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($full_path);
    }

    /**
     * @param      $full_path
     * @param bool $clean_file
     *
     * @return array
     * @throws rsExceptionFile
     */
    public static function makeDirPath($full_path,$clean_file = false)
    {
        $path = $full_path;
        $filename = '';
        if($clean_file)
        {
            $pos = mb_strripos($full_path,'/');
            $path = mb_substr($full_path,0,$pos);
            $filename = mb_substr($full_path,$pos);
        }

        preg_match_all('#([a-zA-Z0-9-_]+)#',$path,$matchsnaya);

        for($i=0;$i<count($matchsnaya[0]);$i++)
        {
            $fl_patharr = array_slice($matchsnaya[0],0,$i+1);
            $tmp_path = implode('/',$fl_patharr).'/';
            if (substr($full_path,0,1)=='/') {
                $tmp_path='/'.$tmp_path;
            }
            if(!is_dir($tmp_path))
            {
                if(!mkdir($tmp_path,0755,true))
                {
                    throw new rsExceptionFile('Path not created - ' . $tmp_path, 3001);
                }
            }
        }

        return array(
            'PATH' => $path,
            'FILE' => $filename
        );
    }

    /**
     * С исправленой ошибкой слеша перед именем файла
     *
     * @param      $full_path
     * @param bool $clean_file
     *
     * @return array
     * @throws rsExceptionFile
     */
    public static function makeDirPathFixed($full_path,$clean_file = false)
    {
        $list=self::makeDirPath($full_path, $clean_file);
        if (!$clean_file) {
            return $list;
        }

        if (substr($list['FILE'],0,1)=='/') {
            $list['FILE']=substr($list['FILE'],1);
        }

        return $list;
    }

    public static function moveDirWithFiles($src, $dst){
        $handle = opendir($src);
        if (!is_dir($dst))
        {
            if(!mkdir($dst))
            {
                throw new rsExceptionFile('Path not created - ' . $dst, 3002);
            }
        }
        while ($file = readdir($handle)){
            if (($file!=".") and ($file!="..")){
                $srcm = $src."/".$file;
                $dstm = $dst."/".$file;
                if (is_dir($srcm)){
                    self::moveDirWithFiles($srcm,$dstm);
                }
                else {
                    if(!copy($srcm, $dstm))
                    {
                        throw new rsExceptionFile('Copy file failed - ' . $srcm . ' to ' . $dstm, 3003);
                    }
                    unlink($srcm);
                }
            }
        }
        closedir($handle);
        rmdir($src);
    }
}