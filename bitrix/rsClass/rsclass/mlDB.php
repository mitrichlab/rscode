<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 18.10.2017
 * Time: 18:04
 */

namespace scarlett;
//require_once __DIR__ . '/ml_dbconn.php';
/**
 * Class mlDB
 * @package scarlett
 */

class mlDB
{
    private static $instance  = null;
    private $db_host;
    private $db_login;
    private $db_password;
    private $db_port;
    private $db_name;
    private $primary_keys;

    private $connect;

    public static function getInstance()
    {
        if(is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function __clone() {}

    private function __construct($settings = null)
    {
      /*  $this->db_host        = mlSettings::$db_host;
        $this->db_login       = mlSettings::$db_login;
        $this->db_password    = mlSettings::$db_password;
        $this->db_port        = mlSettings::$db_port;
        $this->db_name        = mlSettings::$db_name;
*/
        if($settings)
        {
            $this->db_host        = $settings['db_host'];
            $this->db_login       = $settings['db_login'];
            $this->db_password    = $settings['db_password'];
            $this->db_port        = $settings['db_port'];
            $this->db_name        = $settings['db_name'];
        } else {
            $config_file =  __DIR__ . '/../../../bitrix/.settings.php';
            ob_start();
            $ar = include($config_file);
            ob_end_clean();
            $settings_arr = [
                'db_host' => $ar['connections']['value']['default']['host'],
                'db_login' => $ar['connections']['value']['default']['login'],
                'db_password' => $ar['connections']['value']['default']['password'],
                'db_port' => 3306,
                'db_name' => $ar['connections']['value']['default']['database'],
            ];
            $this->db_host        = $settings_arr['db_host'];
            $this->db_login       = $settings_arr['db_login'];
            $this->db_password    = $settings_arr['db_password'];
            $this->db_port        = $settings_arr['db_port'];
            $this->db_name        = $settings_arr['db_name'];
        }

        $connect = new \mysqli($this->db_host, $this->db_login, $this->db_password, $this->db_name, $this->db_port);
        $this->connect = $connect;
        $this->connect->query("SET NAMES 'utf8'");
        $this->connect->query('SET collation_connection = "utf8_unicode_ci"');
        $this->connect->query("SET LOCAL time_zone='".date('P')."'");

    }

    public function __destruct()
    {
        $this->connect->close();
    }

    public function insertOrUpdate($table, $data)
    {
        $new_data = [];
        foreach ($data as $key => $datum)
        {
            if($datum)
            {
                $new_data[$key] = $this->connect->escape_string($datum);
            } else {
                $new_data[$key] = '';
            }
        }

        $query = "INSERT INTO `$table` (" . implode(',' , array_keys($new_data)). ") VALUES ('" . implode("','", array_values($new_data)) . "') \n"
            . "ON DUPLICATE KEY UPDATE \n";
        $duplicate_arr = [];
        foreach ($new_data as $field => $value)
        {
            if(!$value)
            {
                $value = 'NULL';
            }
            $duplicate_arr[] = "`$field` = '$value'";
        }
        $query .= implode(",", $duplicate_arr);

        $this->connect->query($query);
    }

    /**
     * @param string $query
     * @return bool|array
     */
    public function getRow($query)
    {
        $rows = $this->getList($query);

        if(!$rows)
        {
            return false;
        }

        return $rows[0];
    }

    /**
     * @param $table
     * @param $key
     * @param $value
     */
    function remove($table, $key, $value)
    {
        $query = "DELETE FROM $table WHERE $key = '$value'";
        try{
            $this->connect->query($query);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $table
     * @param array $data
     * @return mixed
     */
    public function insert($table, $data)
    {
        $new_data = [];
        foreach ($data as $key => $datum)
        {
            $new_data[$key] = $this->connect->escape_string($datum);
        }

        //Проверка на существование первичного ключа
        $query = "SHOW COLUMNS FROM `$table`";
        $list = $this->getList($query);

        if(isset($this->primary_keys[$table]))
        {
            $primary_key_name = $this->primary_keys[$table];
        } else {
            $primary_key_name = null;
            foreach ($list as $item)
            {
                if(($item['Key'] == 'PRI') && ($item['Extra'] == 'auto_increment'))
                {
                    $primary_key_name = $item['Field'];
                }
            }
            $this->primary_keys[$table] = $primary_key_name;
        }

        $query = "INSERT INTO `$table` (" . implode(',' , array_keys($data)). ") VALUES ('" . implode("','", array_values($data)) . "')";
        $this->connect->query($query);

        if($primary_key_name)
        {
            $id = $this->connect->insert_id;
            if(!$id)
            {
                throw new \Exception($this->connect->error);
            }
            return $id;
        }
        return true;
    }

    /**
     * @param $query
     * @return array|bool
     */
    public function getList($query)
    {
        $ret = [];
        $res = $this->query($query);

        if($res)
        {
            while ($row = $res->fetch_assoc())
            {
                $ret[] = $row;
            }
            $res->close();
            return $ret;
        }
        $res->close();
        return false;
    }

    /**
     * @param $query
     * @param string|array $keyfield
     * @param bool $truncate_key_in_result
     * @return bool|array
     * @throws \Exception
     */
    public function getListByKey($query, $keyfield = 'id', $truncate_key_in_result = true)
    {
        $list = $this->getList($query);

        if(!$list)
        {
            return false;
        }

        if(!isset($list[0][$keyfield]))
        {
            throw new \Exception('Key not found or empty');
        }

        $ret = [];
        foreach ($list as $item)
        {
            if(is_array($keyfield))
            {
                $key = '';
                foreach ($keyfield as $key_row) {
                    $key .= $item[$key_row];
                }
            } else {
                $key = $item[$keyfield];
            }

            if($truncate_key_in_result && !is_array($keyfield))
            {
                unset($item[$keyfield]);
            }

            $ret[$key] = $item;
        }

        if(count($list) != count($ret))
        {
            throw new \Exception('Key not unique value');
        }

        return $ret;
    }

    /**
     * @param $query
     * @return bool|\mysqli_result
     */
    public function query($query)
    {
        try {
            $r = $this->connect->query($query);

            if(!$r)
            {
                throw new \Exception($this->connect->error);
            }
            return $r;
        } catch (\Exception $e) {
            echo '<pre>';
            var_dump($query);
            echo '</pre>';
            echo $e->getMessage();
        }
        return false;
    }

    public function forSQL($val)
	{
    	return $this->connect->escape_string($val);
	}
}