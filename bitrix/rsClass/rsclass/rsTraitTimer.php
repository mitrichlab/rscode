<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 09.07.2017
 * Time: 22:56
 */
namespace rsclass;

trait rsTraitTimer
{
    /**
     * @var float время начала выполнения скрипта
     */
    private static $start = .0;

    /**
     * Начало выполнения
     */
    static function start()
    {
        self::$start = microtime(true);
    }

    /**
     * Разница между текущей меткой времени и меткой self::$start
     * @return float
     */
    static function finish()
    {
        return microtime(true) - self::$start;
    }
}