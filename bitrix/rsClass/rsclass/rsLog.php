<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 12.07.2017
 * Time: 0:41
 */

namespace rsclass;

/**
 * Class rsLog
 * @package rsclass
 */
class rsLog
{
    private $log_path;
    private $fp = null;

    /**
     * @var rsLog[] Мвссив экземпляров логгеров для каждого пути
     */
    private static $instance = [];

    /**
     * rsLog constructor.
     *
     * @param string $log_path
     *
     * @throws rsExceptionLog
     */
    protected function __construct($log_path = '')
    {
        $this->setLogpath($log_path);
    }

    /**
     * Возвращает "настоящий" путь к логу, то есть для пустого пути возвращает реальное дефолтное значение
     * @param $log_path
     *
     * @return string
     */
    protected static function getRealLogPath($log_path)
    {
        if ($log_path) {
            return $log_path;
        }

        if (isset($_SERVER['DOCUMENT_ROOT'])) {
            return $_SERVER['DOCUMENT_ROOT'] . '/rslog.log';
        }

        return __DIR__ . '/rslog.log';
    }

    protected function __clone()
    {
    }

    /**
     * Если мы устанавливаем экземпляру логгера вручную другой путь, то фактически мы должны старый экземпляр сделать копией этого
     * Иначе надо использовать getInstance с новым путём
     *
     * Поддержка STDOUT и STDERR
     *
     * @param $log_path
     * @throws rsExceptionLog
     */
    public function setLogpath($log_path)
    {
        $log_path=self::getRealLogPath($log_path);

        $oldLogPath = $this->log_path;

        if ($log_path === STDOUT || $log_path === STDERR) {
            $this->fp = $log_path;

            self::$instance[$this->log_path] = $this;

            /**
             * Экземпляр только что создан, нет старого логгера
             */
            if (!$oldLogPath) {
                return;
            }

            self::$instance[$oldLogPath] = $this;
            return;
        }

        try {
            $list = \rsClass::makeDirPathFixed($log_path, true);
            $this->log_path = $list['PATH'] . DIRECTORY_SEPARATOR . $list['FILE'];
            if ($this->fp) {
                fclose($this->fp);
            }
            $this->fp = fopen($this->log_path, 'ab');
            self::$instance[$this->log_path] = $this;
            if ($oldLogPath) {
                self::$instance[$oldLogPath] = $this;
            }
        } catch (rsExceptionFile $e) {
            throw new rsExceptionLog($e->getMessage(), $e->getMessage());
        }
    }

    /**
     * Получает или создает экземпляр логгера для заданного пути
     *
     * @param null $log_path
     *
     * @return rsLog
     * @throws rsExceptionLog
     */
    public static function getInstance($log_path = null)
    {
        $log_path=self::getRealLogPath($log_path);

        if (!isset(self::$instance[$log_path])) {
            self::$instance[$log_path] = new self($log_path);
        }

        return self::$instance[$log_path];
    }

    public function write($message)
    {
        if (!is_string($message)) {
            $message = var_export($message, true);
        }

        $text = date('Y-m-d H:i:s') . ' ' . $message . "\n";
        //TODO Add trace
        fwrite($this->fp, $text);
    }
}