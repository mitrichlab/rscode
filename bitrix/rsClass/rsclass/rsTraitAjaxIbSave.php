<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 10.07.2017
 * Time: 23:13
 */

namespace rsclass;

trait rsTraitAjaxIbSave
{
    private $IBLOCK_ID;
    private $PROPS;
    private $FIELDS = array(
        'ACTIVE' => 'Y',
        'IBLOCK_SECTION_ID' => false
    );

    /**
     * @param $ID int
     * @throws rsExceptionAjax
     */
    public function setIblockID($ID)
    {
        if(!intval($ID))
        {
            throw new rsExceptionAjax('Iblock ID must be integer', 5001);
        }
        $ib = \CIBlock::GetByID($ID);
        if(!$ib->Fetch())
        {
            throw new \rsclass\rsExceptionAjax('Iblock with ID=' . $ID . ' not found', 5002);
        }
        $this->IBLOCK_ID = intval($ID);
        $this->FIELDS['IBLOCK_ID'] = $ID;
    }

    /**
     * @param $fields array
     */
    public function setFields($fields)
    {
        foreach ($fields as $field => $value) {
            if($field == 'IBLOCK_ID')
            {
                $this->setIblockID($value);
                continue;
            }
            $this->FIELDS[$field] = $value;
        }
    }

    /**
     * @param $props array
     */
    public function setProperties($props)
    {
        foreach ($props as $key => $prop) {
            $this->FIELDS['PROPERTY_VALUES'][$key] = $prop;
        }
    }

    public function setPropertiesFromRequest($prefix = 'PROP_', $method = 'REQUEST')
    {
        $method = strtoupper($method);
        switch ($method)
        {
            case 'POST':
                $data = $_POST;
                break;
            case 'GET':
                $data = $_GET;
                break;
            case 'REQUEST':
                $data = $_REQUEST;
                break;
            default:
                throw new rsExceptionAjax('Not allowed request type. Allow POST|GET|REQUEST', 5005);
                break;

        }
        foreach ($data as $key => $prop) {
            if(strpos($key,$prefix) === 0)
            {
                $this->FIELDS['PROPERTY_VALUES'][$key] = $prop;
            }
        }
    }

    public function saveIbRow()
    {
        $el = new \CIBlockElement();
        $EL_ID = $el->Add($this->FIELDS);
        if(!$EL_ID)
        {
            throw new \rsclass\rsExceptionAjax($el->LAST_ERROR, 5003);
        }
        return $EL_ID;
    }
}