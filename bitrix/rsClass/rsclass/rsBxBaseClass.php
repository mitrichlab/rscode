<?php
namespace rsclass;
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 09.07.2017
 * Time: 22:29
 */
abstract class rsBxBaseClass
{

    function __construct()
    {
    }

    /**
     * @param $size
     * @param string $unit
     * @return string
     */
    private static function humanFileSize($size,$unit="")
    {
        if( (!$unit && $size >= 1<<30) || $unit == "ГБ")
            return number_format($size/(1<<30),2)."ГБ";
        if( (!$unit && $size >= 1<<20) || $unit == "МБ")
            return number_format($size/(1<<20),2)."МБ";
        if( (!$unit && $size >= 1<<10) || $unit == "кБ")
            return number_format($size/(1<<10),2)."кБ";
        return number_format($size)." Б";
    }

    /**
     * @param null $id
     * @param string $upload_dir
     * @return mixed
     */
    public static function getImageArrByID($id = null, $upload_dir = '/upload/')
    {
        if (!$id) {
            throw new rsBxException('File ID not set',10001);
        }

        $img_DBOB = CFile::GetByID($id);

        $img_arr = $img_DBOB->Fetch();
        if(!$img_arr)
        {
            throw new rsBxException('File not found by ID',10002);
        }

        $img_arr['HUMAN_SIZE'] = self::humanFileSize($img_arr['FILE_SIZE']);

        $img_arr['SRC'] = $upload_dir . $img_arr['SUBDIR'] . '/' . $img_arr['FILE_NAME'];

        $img_arr['TYPE'] = strtolower(substr($img_arr['ORIGINAL_NAME'],strrpos($img_arr['ORIGINAL_NAME'],'.') + 1));

        $img_arr['REAL_TYPE'] = $img_arr['TYPE'];

        switch($img_arr['TYPE'])
        {
            case 'rar':
            case '7z':
            case 'arj':
            case 'gz':
            case 'gzip':
            case 'tar.gz':
            case 'bz':
            case 'tar.bz':
            case 'tar':
                $img_arr['TYPE'] = 'zip';
                break;
            case 'docx':
            case 'rtf':
            case 'txt':
                $img_arr['TYPE'] = 'doc';
                break;
            case 'xlsx':
            case 'odt':
                $img_arr['TYPE'] = 'xls';
                break;
            case 'pptx':
            case 'odp':
                $img_arr['TYPE'] = 'ppt';
                break;
        }

        $img_arr['AJAX_URL'] = '/ajax/files/'.$id.'/'.$img_arr['EXTERNAL_ID'].'/';

        return $img_arr;
    }

    /**
     * @param int $length
     * @return string
     */
    public static function getRand($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}