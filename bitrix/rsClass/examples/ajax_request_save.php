<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 10.07.2017
 * Time: 20:24
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$rsAjax = rsClass::getInstance(); //Получили инстанс
$rsAjax->setHeader('html'); //Установили дефолтный заголовок (text|json)
$rsAjax->isAjax(0, 'Not XHR Request', '', 'break', true); //Проверка на XHR
//$rsAjax->setAddParamType('append'); //Если отладочные поля нужно добавлять в конец массива, установим флаг append, но для отладки удобно выводить добавляемые значения в начало массива
//Установили дефолтные значения, необязательно
$rsAjax->setState(0);
$rsAjax->setHtml('');
$rsAjax->setText('Not allow operation');

//Добавили поля для отладки
$rsAjax->olo = 'nlo';
$rsAjax->lalala = [123,456];

//Удалили поле для отладки, если уже не надо
unset($rsAjax->olo);

//сохраним
try{
    //Установили ID инфоблока, необязательно, можно привычно передать его в $fields с ключом IBLOCK_ID
    $rsAjax->setIblockID(1);

    //Собрали поля, все доп поля уже прописаны, но можно их переназначить в массиве $fields
    $fields = array(
        'NAME' => 'Тестовая запись'
    );
    //Сохранили поля
    $rsAjax->setFields($fields);

    //СОбрали свойства
    $props = array(
        'TXT_1' => 'Какой-то текст',
        'TXT_2' => 'Еще какой-то текст'
    );
    //Сохранили свойства
    $rsAjax->setProperties($props);
    $rsAjax->setPropertiesFromRequest($field_prefix = 'PROP_');
    //Сохранили элемент инфоблока
    $ID = $rsAjax->saveIbRow();

} catch (rsclass\rsExceptionAjax $e) {
    $rsAjax->setState(0);
    $rsAjax->setText( $e->getMessage());
    $rsAjax->error = $e->getTrace();
    $rsAjax->error_id = $e->getCode();
    $rsAjax->showResponse();
}


$rsAjax->setState(1);
$rsAjax->setText('Ok');
$rsAjax->showResponse();

