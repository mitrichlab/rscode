<?php
/**
 * Created by PhpStorm.
 * User: mitrich
 * Date: 09.07.2017
 * Time: 22:43
 */
spl_autoload_register('rsAutoload');

class rsClass extends \rsclass\rsBxBaseClass
{
    private static $instance = null;
    use rsclass\rsTraitFile;
    use rsclass\rsTraitTimer;
    use rsclass\rsTraitAjax;

    function __construct()
    {
        parent::__construct();
    }

    protected function __clone(){}

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

function rsAutoload($path)
{
    $path = str_replace("\\",DIRECTORY_SEPARATOR,$path);
    $file = __DIR__ . DIRECTORY_SEPARATOR . "{$path}.php";
    if(file_exists( $file))
    {
        require_once $file;
    }
}