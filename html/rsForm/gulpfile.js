var gulp = require('gulp'),
  sass = require('gulp-sass'),
  webserver = require('gulp-webserver');

gulp.task('sass', function () {
  return gulp.src('app/_scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
})

gulp.watch('app/_scss/*.scss', ['sass']);

gulp.task('webserver', function() {
  gulp.src('')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true,
      fallback: 'index.html'
    }));
});


gulp.task('default', ['sass', 'webserver']);